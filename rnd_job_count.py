import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.mark.parametrize("department, expected_count", [("Research & Development", 12)])
def test_job_count(department, expected_count):
    # accessing the page, maximizing browser window
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.get('https://cz.careers.veeam.com/vacancies')

    # filtering available R&D jobs by clicking through "All departments" dropdown list
    department_button = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.ID, "sl")))
    department_button.click()

    rnd_button = WebDriverWait(driver, 10).until(
        EC.element_to_be_clickable((By.XPATH,  f"//*[@class='dropdown-item' and text()='{department}']")))
    rnd_button.click()

    # counting how many times R&D XPATH is appearing on loaded the page
    count = len(WebDriverWait(driver, 10).until(EC.presence_of_all_elements_located(
        (By.XPATH, f"//span[@class='text-placeholder' and text()='{department}']"))))

    assert count == expected_count

    driver.quit()
